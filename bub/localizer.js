import en from './en.json';
import de from './de.json';

export default class localizer {
  constructor(locale = 'de') {
    this.locale = locale;
  }

  w() {
    switch (this.locale) {
      case 'en':
        return en;
        break;
      case 'de':
        return de;
        break;
      default:
        return en;
    }
  }
}
