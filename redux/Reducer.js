import {combineReducers} from 'redux';
import {HANDLE_SCROLL, NEW_OWAG} from './types';

const INITIAL_STATE = {
  owgz: [],
  addBtnOpacity: 1,
  creating: false,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HANDLE_SCROLL: {
      const {addBtnOpacity} = state;
      let newOpacity = addBtnOpacity;
      const {x} = action.payload;

      if (x <= 1) {
        if (newOpacity != 1) {
          newOpacity = 1;
        }
      } else if (x < 1.25) {
        const y = (x - 1) * 5;
        newOpacity = 1 - y;
      } else if (newOpacity > 0) {
        newOpacity = 0;
      }

      return {...state, newOpacity};
    }
    case NEW_OWAG: {
      const {owgz} = state;
      const {name, phone, money, desc} = action.payload;

      for (var i in owgz) {
        if (owgz[i].nr == phone) {
          var owg = owgz[i];
          owg.acts.unshift({money, desc, time: Date()});
          owg.balance = parseInt(owg.balance) + parseInt(money) + '';
          owgz[i] = owg;

          return {owgz, ...state};
        }
      }

      const owgNew = {
        name,
        nr: '' + owgz.length,
        balance: money,
        acts: [{money, desc, time: Date()}],
      };

      let newOwgz = [owgNew, ...owgz];

      console.log(newOwgz);

      return {...state, owgz: newOwgz};
    }
    default:
      return state;
  }
};

export default combineReducers({
  reducer,
});
