import {HANDLE_SCROLL, NEW_OWAG} from './types';

export const handleScroll = (x) => ({
  type: HANDLE_SCROLL,
  payload: {x},
});
export const newOwag = (name, phone, money, desc = '') => ({
  type: NEW_OWAG,
  payload: {name, phone, money, desc},
});
