/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, ScrollView, View, Text, StatusBar} from 'react-native';
import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {Header} from 'react-native-elements';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {createStore} from 'redux';
import reducer from './redux/Reducer';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import {style} from './view/style';

import Owagz from './view/Owagz';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(persistedReducer);
const persistor = persistStore(store);

const App: () => React$Node = () => {
  return (
    <View style={style.app}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView style={{display: 'flex'}}>
            <Owagz />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    </View>
  );
};

export default App;

// <Header
//   // statusBarProps={{barStyle: 'light-content'}}
//   // barStyle="light-content" // or directly
//   centerComponent={{text: "0wag'z", style: {color: '#fff', fontSize: 28}}}
//   // containerStyle={{
//   //   backgroundColor: '#3D6DCC',
//   //   justifyContent: 'space-around',
//   // }}
// />
