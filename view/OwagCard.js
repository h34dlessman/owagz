/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
  TextInput,
  Keyboard,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {newOwag} from '../redux/Actions';

import localizer from '../bub/localizer';
import {style} from './style';

class OwagCard extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      newAmount: '',
      newDesc: '',
      owg: props.owg,
      expandedOwgCell: null,
    };
    //console.log('CONSTR', props);

    this.l = new localizer();
  }

  componentDidUpdate(nextProps) {
    //console.log('componentDidUpdate', nextProps);
    if (this.state.owg !== nextProps.owg) {
      this.setState({owg: nextProps.owg});
    }
  }

  updateOwg(x) {
    const {newOwag} = this.props;
    const {owg, newAmount, newDesc} = this.state;
    newOwag(owg.name, owg.nr, newAmount * x, newDesc);
    this.setState({newAmount: '', newDesc: '', expanded: false});
  }

  expand() {
    const {iExpanded} = this.props;
    const {expanded} = this.state;

    iExpanded(() => {
      this.collapse();
    });

    this.setState({expanded: !expanded, newAmount: '', newDesc: ''});
  }

  collapse() {
    this.setState({expanded: false, newAmount: '', newDesc: ''});
  }

  expandedHeight() {
    const {owg} = this.state;
    return 212 + 62 * owg.acts.length;
  }

  render() {
    const {expanded, owg} = this.state;

    return (
      <TouchableWithoutFeedback onPress={() => this.expand()}>
        <View>
          <View
            style={{
              ...style.cell,
              ...(expanded ? {height: this.expandedHeight()} : {}),
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                adjustContent: 'center',
                height: style.cell.height,
              }}>
              <View style={{...style.textCenterer, flex: 3}}>
                <Text
                  adjustsFontSizeToFit
                  numberOfLines={1}
                  style={style.owgName}>
                  {owg.name}
                </Text>
              </View>
              <View style={{...style.textCenterer, flex: 1}}>
                <Text
                  adjustsFontSizeToFit
                  numberOfLines={1}
                  style={style.owgBalance}>
                  {owg.balance.toLocaleString()}
                  {this.l.w().moneySign}
                </Text>
              </View>
            </View>
            {expanded ? this.renderActions() : null}
            {expanded ? this.renderList() : null}
          </View>
          {expanded ? this.renderExpandedBG() : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  renderExpandedBG() {
    return (
      <View
        style={{
          position: 'absolute',
          top: style.cell.height,
          height: this.expandedHeight() - style.cell.height + 6,
          width: Dimensions.get('window').width - 16,
          zIndex: -1,
          backgroundColor: 'rgba(25,25,25,.25)',
          right: 8,
          left: 8,
          borderRadius: style.cell.borderRadius,
        }}
      />
    );
  }

  renderActions() {
    const {newAmount, newDesc} = this.state;
    return (
      <View style={style.cellActionContainer}>
        <TextInput
          keyboardType={'number-pad'}
          value={newAmount}
          onChangeText={(text) => {
            let newAmount = text;
            if (newAmount.length > 1) {
              let d = newAmount[newAmount.length - 1];
              newAmount = newAmount.substring(0, newAmount.length - 2);
              newAmount += d + this.l.w().moneySign;
            } else if (newAmount.length == 1) {
              newAmount += this.l.w().moneySign;
            } else {
              newAmount = '';
            }
            this.setState({newAmount});
          }}
          placeholder={'0' + this.l.w().moneySign}
          style={{...style.addCardMoney, ...style.owgMoney}}
          blurOnSubmit={false}
          returnKeyType={'next'}
          onSubmitEditing={() => {
            this.descInput.focus();
          }}
        />
        <TextInput
          ref={(input) => {
            this.descInput = input;
          }}
          value={newDesc}
          onChangeText={(newDesc) => this.setState({newDesc})}
          placeholder={'(for)'}
          style={{...style.addCardDesc, ...style.owgDesc}}
          blurOnSubmit={false}
          returnKeyType={'done'}
          onSubmitEditing={() => Keyboard.dismiss()}
        />

        <View style={{display: 'flex', flexDirection: 'row'}}>
          <TouchableWithoutFeedback onPress={() => this.updateOwg(1)}>
            <View
              style={{
                ...style.addCardButton,
                ...style.addCardButtonGive,
                ...style.owgButton,
              }}>
              <Text
                style={{...style.addCardButtonText, ...style.owgButtonText}}>
                give
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => this.updateOwg(-1)}>
            <View
              style={{
                ...style.addCardButton,
                ...style.addCardButtonGet,
                ...style.owgButton,
              }}>
              <Text
                style={{...style.addCardButtonText, ...style.owgButtonText}}>
                get
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }

  renderList() {
    const {disableScroll} = this.props;
    const {owg} = this.state;

    return (
      <View
        style={{
          flex: 2,
          overflow: 'hidden',
          width: '100%',
          paddingTop: 2,
        }}>
        <FlatList
          style={{
            overflow: 'hidden',
          }}
          renderItem={(owgAction) => this.renderOwgActionCell(owgAction.item)}
          data={owg.acts}
          keyExtractor={(item, index) => index + ''}
        />
      </View>
    );
  }

  renderOwgActionCell(owgAction) {
    const {time, money, desc} = owgAction;
    return (
      <View
        style={{
          height: 50,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginVertical: 6,
          paddingHorizontal: 12,
          borderRadius: 6,
          backgroundColor: 'rgba(0,0,0,.06)',
        }}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            flex: 3,
          }}>
          <Text style={{fontSize: 16, fontWeight: '500'}}>{desc}</Text>
          <Text style={{fontSize: 16}}>{new Date(time).toDateString()}</Text>
        </View>
        <Text style={{fontSize: 26, flex: 2, textAlign: 'right'}}>
          {money + this.l.w().moneySign}
        </Text>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({newOwag}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(OwagCard);
