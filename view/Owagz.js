/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {View, Text, FlatList, TouchableWithoutFeedback} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {handleScroll} from '../redux/Actions.js';

import localizer from '../bub/localizer';
import {style} from './style';
import AddButton from './AddButton';
import AddCard from './AddCard';
import OwagCard from './OwagCard';

class Owagz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      creating: false,
      showAdd: true,
      collapse: null,
      enableMainListScroll: true,
    };

    this.l = new localizer();
  }

  showAddCard() {
    console.log('TRIGGER ADD');
    this.setState({creating: true});
  }

  hideAddCard() {
    this.setState({creating: false});
  }

  refresh() {}

  handleScroll(event) {
    const {y} = event.nativeEvent.contentOffset;
    const x = style.cell.height / 2;
    if (y <= x * 2) {
      this.addButton.setOpacity(1);
    } else if (y <= 3 * x) {
      this.addButton.setOpacity(1 - (y - x * 2) / x);
    } else {
      this.addButton.setOpacity(0);
    }
  }

  disableMainListScroll() {
    console.log('disableScroll');
    this.setState({enableMainListScroll: false});
  }

  render() {
    const {refreshing, creating, showAdd} = this.state;
    const {owgz} = this.props;

    console.log(this.state.ooo);
    return (
      <View style={style.main}>
        {owgz != null && owgz.length > 0
          ? this.renderOwagz()
          : this.renderEmpty()}
        {showAdd ? (
          <AddButton
            triggerAdd={() => this.showAddCard()}
            childRef={(ref) => (this.addButton = ref)}
          />
        ) : null}
        {creating ? <AddCard dismiss={() => this.hideAddCard()} /> : null}
      </View>
    );
  }

  renderOwagz() {
    const {refreshing, enableMainListScroll} = this.state;
    const {owgz} = this.props;
    return (
      <FlatList
        nestedScrollEnabled={true}
        renderItem={(owg) => this.renderOwg(owg)}
        style={{width: '100%', height: '100%', overflow: 'visible'}}
        data={owgz}
        keyExtractor={(item) => item.nr}
        refreshing={refreshing}
        onRefresh={() => this.refresh()}
        onScroll={(event) => this.handleScroll(event)}
        scrollEnabled={enableMainListScroll}
      />
    );
  }

  renderOwg(item) {
    return (
      <OwagCard
        owg={item.item}
        iExpanded={(coll) => {
          this.iExpanded(coll);
        }}
        disableScroll={() => {
          this.disableMainListScroll();
        }}
      />
    );
  }

  iExpanded(newCollapse) {
    const {collapse} = this.state;

    if (collapse && collapse != null) {
      collapse();
    }
    this.setState({collapse: newCollapse});
  }

  renderEmpty() {
    return <Text style={style.empty}>tap "New" to track a new 0wag</Text>;
  }
}

const mapStateToProps = (state) => {
  const {reducer} = state;
  const {owgz} = reducer;

  return {owgz};
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      handleScroll,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Owagz);
