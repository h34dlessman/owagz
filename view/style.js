export const style = {
  app: {
    backgroundColor: '#e8e7e5',
  },
  main: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    overflow: 'visible',
  },
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    CELL / OWAGECARD
  cell: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(255,255,255,.85)',

    height: 120,
    borderRadius: 4,
    paddingHorizontal: 24,
    marginVertical: 6,
    marginHorizontal: 8,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  cellExpanded: {
    height: 360,
  },
  owgName: {
    fontSize: 34,
    paddingRight: 8,
    textAlignVertical: 'center',
  },
  owgBalance: {
    fontSize: 34,
  },
  cellActionContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 80,
  },
  empty: {
    textAlign: 'center',
    fontSize: 28,
  },

  owgButton: {
    flex: 0,
    paddingHorizontal: 4,
  },
  owgMoney: {
    maxWidth: '30%',
  },
  owgDesc: {
    maxWidth: '30%',
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    ADD BUTTON
  addButtonContainer: {
    position: 'absolute',
    bottom: '2%',
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    width: 110,
    height: 65,
    backgroundColor: 'rgba(255,255,255,0.8)',
    borderColor: '#4f887d',
    borderRadius: 19,
    borderWidth: 3,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.3,
    shadowRadius: 1,

    elevation: 2,
  },
  addButtonText: {
    textAlign: 'center',
    color: '#5d8682',
    fontWeight: '600',
    fontSize: 30,
  },
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    ADD CARD
  addCardContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    left: 0,
    right: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 200,

    elevation: 4,
  },

  addCard: {
    padding: 8,
    marginTop: '6%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: 265,
    width: 500,
    maxWidth: '85%',
    backgroundColor: 'rgba(255,255,255,1)',

    borderRadius: 8,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.24,
    shadowRadius: 18,

    elevation: 4,
  },
  addCardName: {
    textAlign: 'center',
    color: '#777',
    fontWeight: '600',
    fontSize: 35,
  },
  addCardPhone: {
    textAlign: 'center',
    color: '#777',
    fontWeight: '600',
    fontSize: 30,
  },
  addCardMoneyWrap: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  addCardMoney: {
    textAlign: 'center',
    color: '#777',
    fontWeight: '600',
    fontSize: 45,
  },
  addCardDesc: {
    textAlign: 'center',
    color: '#777',
    fontWeight: '300',
    fontSize: 35,
  },
  addCardButtonWrap: {
    paddingTop: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%',
  },
  addCardButton: {
    flex: 2,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addCardButtonGive: {
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#fd7d73',
  },
  addCardButtonGet: {
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor: '#b7d693',
  },
  addCardButtonText: {
    textAlign: 'center',
    color: '#fefefe',
    fontWeight: '600',
    fontSize: 26,
    padding: 8,
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    STUFF
  textCenterer: {
    flex: 1,
    justifyContent: 'center',
  },

  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
  //↓↓↓↓↓↓↓↓↓↓↓↓↓↓    COLOOOOORS

  emptyHighlight: {
    color: '#f35252',
  },
};
