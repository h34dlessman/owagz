/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {style} from './style';

class AddButton extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      opacity: 1,
    };
  }
  componentDidMount() {
    const {childRef} = this.props;
    childRef(this);
  }
  componentWillUnmount() {
    const {childRef} = this.props;
    childRef(undefined);
  }

  componentDidUpdate(nextProps) {
    if (this.props.addBtnOpacity != nextProps.addBtnOpacity) {
      this.setState({opacity: nextProps.addBtnOpacity});
    }
  }

  setOpacity(opacity) {
    this.setState({opacity});
  }

  render() {
    const {triggerAdd} = this.props;
    const {opacity} = this.state;

    return (
      <View style={style.addButtonContainer}>
        <TouchableWithoutFeedback onPress={triggerAdd}>
          <View style={{...style.addButton, opacity}}>
            <Text style={style.addButtonText}>new</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {reducer} = state;
  const {addBtnOpacity} = reducer;
  return {addBtnOpacity};
};
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(AddButton);
