/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
  TextInput,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {newOwag} from '../redux/Actions';

import localizer from '../bub/localizer';
import {style} from './style';

class AddCard extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone: '',
      money: '',
      desc: '',
      emptyHighlight: false,
      yOffset: 0,
    };

    this.l = new localizer();

    Keyboard.addListener('keyboardWillHide', this.keyboardDismiss, this);
    Keyboard.addListener('keyboardWillShow', this.keyboardOpen, this);
  }

  componentDidUpdate(nextProps) {
    if (this.props.addBtnOpacity != nextProps.addBtnOpacity) {
      this.setState({opacity: nextProps.addBtnOpacity});
    }
  }

  createNew(x) {
    const {newOwag, dismiss} = this.props;
    const {name, phone, money, desc} = this.state;

    if (name == '' || money == '') {
      this.setState({emptyHighlight: true});
      return;
    }
    newOwag(name, phone, parseInt(money) * x, desc);
    dismiss();
  }

  backgroundPress() {
    const {dismiss} = this.props;
    if (this.state.yOffset != 0) {
      this.setState({yOffset: 0});
      Keyboard.dismiss();
    } else dismiss();
  }

  keyboardDismiss() {
    this.state.yOffset != 0 && this.setState({yOffset: 0});
  }

  keyboardOpen() {
    this.state.yOffset == 0 && this.setState({yOffset: 200});
  }

  render() {
    const {triggerAdd} = this.props;
    const {yOffset, name, phone, money, desc, emptyHighlight} = this.state;

    return (
      <View
        onStartShouldSetResponder={(evt) => true}
        onResponderGrant={(evt) => {
          this.backgroundPress();
        }}
        style={style.addCardContainer}>
        <View
          onStartShouldSetResponder={(evt) => true}
          style={{...style.addCard, marginBottom: yOffset}}>
          <TextInput
            value={name}
            onChangeText={(name) => this.setState({name})}
            placeholder={this.l.w().name}
            style={style.addCardName}
            placeholderTextColor={
              emptyHighlight && name == '' ? style.emptyHighlight.color : null
            }
            blurOnSubmit={false}
            returnKeyType={'next'}
            onSubmitEditing={() => {
              this.moneyInput.focus();
            }}
          />
          {/*
          <TextInput
            value={phone}
            onChangeText={(phone) => this.setState({phone})}
            placeholder={this.l.w().phone}
            style={style.addCardPhone}
          />
        */}
          <View style={style.addCardMoneyWrap}>
            <TextInput
              keyboardType={'number-pad'}
              ref={(input) => {
                this.moneyInput = input;
              }}
              value={money}
              onChangeText={(text) => {
                let money = text;
                if (money.length > 1) {
                  let d = money[money.length - 1];
                  money = money.substring(0, money.length - 2);
                  money += d + this.l.w().moneySign;
                } else if (money.length == 1) {
                  money += this.l.w().moneySign;
                } else {
                  money = '';
                }
                this.setState({money});
              }}
              placeholder={'0' + this.l.w().moneySign}
              style={style.addCardMoney}
              placeholderTextColor={
                emptyHighlight && money == ''
                  ? style.emptyHighlight.color
                  : null
              }
              blurOnSubmit={false}
              returnKeyType={'done'}
              onSubmitEditing={() => {
                this.descInput.focus();
              }}
            />
            <TextInput
              ref={(input) => {
                this.descInput = input;
              }}
              value={desc}
              onChangeText={(desc) => this.setState({desc})}
              placeholder={'(for)'}
              style={style.addCardDesc}
              blurOnSubmit={false}
              returnKeyType={'done'}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
            <View style={style.addCardButtonWrap}>
              <View style={{flex: 1}} />
              <TouchableWithoutFeedback onPress={() => this.createNew(1)}>
                <View
                  style={{
                    ...style.addCardButton,
                    ...style.addCardButtonGive,
                  }}>
                  <Text style={style.addCardButtonText}>give</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.createNew(-1)}>
                <View
                  style={{...style.addCardButton, ...style.addCardButtonGet}}>
                  <Text style={style.addCardButtonText}>get</Text>
                </View>
              </TouchableWithoutFeedback>
              <View style={{flex: 1}} />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {reducer} = state;
  const {addBtnOpacity} = reducer;
  return {addBtnOpacity};
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({newOwag}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(AddCard);
